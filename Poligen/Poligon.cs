/*
 * Copyright (c) 2014 Santi Ruiz <starfly1570@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

using System;
using System.Collections;
namespace Poligen
{
	public class Poligon
	{
		
		private ArrayList vertex;
		private int vertexCount;
		private float size;
		private bool invert;
		private float xOffset;
		private float yOffset;
		
		public Poligon (int sides, float size, float xOffset, float yOffset)
		{
			if ( sides <= 2 )
				throw new ApplicationException("Not valid poligon (sides number less than 3)");
			
			this.vertexCount = sides;
			this.vertex = new ArrayList();
			this.invert = false;
			this.size = size;
			this.xOffset = xOffset;
			this.yOffset = yOffset;
			this.CreatePoligon();
		}
		
		public Poligon(int sides, float size, bool invert)
			: this(sides,size,0,0)
		{
			this.invert = invert;
		}
		
		public Poligon (int sides, float size, float xOffset, float yOffset, bool invert)
			: this(sides,size,xOffset,yOffset)
		{
			this.invert = invert;
		}
		
		/*
		 * The mathematical algorithm that create the poligon.
		 */
		private void CreatePoligon()
		{
			float gradesPerSide = (this.vertexCount - 2) * 180 / this.vertexCount;
			float decrement = (float) (gradesPerSide * 2 / (this.vertexCount - 2) * Math.PI / 180);
			this.vertex.Add(new Point(this.xOffset, this.yOffset));
			float angle = (float) (gradesPerSide * Math.PI / 180);
			
			for ( int i = 1; i < this.vertexCount; i++ ) {
				float x = (float) Math.Cos(angle) * this.size;
				float y = (float) Math.Sin(angle) * this.size;
				
				Point previousPoint = this.vertex[i-1] as Point;
				this.vertex.Add(new Point(previousPoint.GetX() + x, previousPoint.GetY() + y));
				
				angle = angle - decrement;
			}
		}
		
		public Point GetPoint(int index)
		{
			if ( index >= this.vertexCount ) {
				throw new ApplicationException("index \"" + index 
				                               + "\" is greater than vertex count \"" 
				                               + this.vertexCount + "\"");
			} else if ( index < 0 ) {
				throw new ApplicationException("index \"" + index 
				                               + "\" is less than 0");
			}
			
			Point point = this.vertex[index] as Point;
			Point initialPoint = this.vertex[0] as Point; 
			
			if ( invert && point.GetY() > initialPoint.GetY() ) {
				float newY = 2 * initialPoint.GetY() - point.GetY();
				point.SetY(newY);
			}
			
			return point;
		}
		
		public int GetVertexCount()
		{
			return this.vertexCount;
		}
		
		public bool IsInvert()
		{
			return this.invert;
		}
		
		public void SetInvert(bool invert)
		{
			this.invert = invert;
		}
		
		public float GetXOffset()
		{
			return this.xOffset;
		}
		
		public float GetYOffset()
		{
			return this.yOffset;
		}
		
		public void SetOffset(float xOffset, float yOffset)
		{
			this.xOffset = xOffset;
			this.yOffset = yOffset;
		}
		
		public override string ToString ()
		{
			string result = "[";
			for ( int i = 0; i < this.vertexCount; i++ ) {
				Point p = this.GetPoint(i);
				result += p.ToString() + ";";
			}
			result = result.Substring(0,result.Length-1) + "]";
			return result;
		}
	}
}